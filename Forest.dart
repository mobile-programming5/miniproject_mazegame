import 'Obj.dart';

class Forest extends Obj {
  late int damage;
  Forest(super.symbol, super.row, super.col, int damage) {
    this.damage = damage;
  }
  int getDamage() {
    return damage;
  }
}
