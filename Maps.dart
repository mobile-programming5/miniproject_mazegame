import 'dart:ffi';
import 'dart:io';
import 'dart:math';

import 'Characters.dart';
import 'Forest.dart';
import 'Obj.dart';
import 'Wall.dart';

class Maps {
  late Characters currentCharac;
  late Characters killer;
  late Characters survivor;
  var emoGoal = '\u{1F6A9} ';
  var maze = List.generate(20, (i) => List.filled(20, "   ", growable: false),
      growable: false);
  final List object = List.generate(250, (index) => null);
  int objCount = 0;
  Maps() {
    var damage = Random().nextInt(5) + 1;
    this.killer = new Characters(' K ', 5, 8, this, 100, damage);
    this.survivor = new Characters(' S ', 5, 12, this, 20, 0);
    this.currentCharac = survivor;
  }
  addObj(Obj obj) {
    object[objCount] = obj;
    objCount++;
  }

  Characters getCurrentCharac() {
    return currentCharac;
  }

  setCharacter(
      Characters survivor, Characters killer, Characters currentCharac) {
    this.survivor = survivor;
    this.killer = killer;
    this.currentCharac = survivor;
    addObj(survivor);
    addObj(killer);
  }

  setGoal() {
    Characters goal = new Characters(emoGoal, 18, 11, this, 0, 0);
    addObj(goal);
  }

  switchCharac() {
    if (currentCharac == killer) {
      currentCharac = survivor;
    } else {
      currentCharac = killer;
    }
  }

  bool setMaze(int row, int col) {
    maze[row][col] = "   ";
    return true;
  }

  bool inMap(int x, int y) {
    if ((x >= 0 && x < 20) && (y >= 0 && y < 20)) {
      return true;
    }
    return false;
  }

  bool isWall(int x, int y) {
    for (int i = 0; i < objCount; i++) {
      if (object[i] is Wall && object[i].isOnMap(x, y)) {
        return true;
      }
    }
    return false;
  }

  bool isForest(int x, int y) {
    for (int i = 0; i < objCount; i++) {
      if (object[i] is Forest && object[i].isOnMap(x, y)) {
        int hp = currentCharac.getHp();
        int dForest = object[i].getDamage();
        int damage = hp - dForest;
        currentCharac.setHp(damage);
        return true;
      }
    }
    return true;
  }

  bool isCharacter() {
    return getCurrentCharac() == killer;
  }

  bool isSurvivor(int x, int y) {
    if (maze[x][y] == survivor.getSymbol() && getCurrentCharac() == killer) {
      int damageK = survivor.getHp() - currentCharac.getDamage();
      survivor.setHp(damageK);
      return true;
    }
    return true;
  }

  bool isKiller(int x, int y) {
    if (maze[x][y] == killer.getSymbol() && getCurrentCharac() == survivor) {
      int damageK = currentCharac.getHp() - killer.getDamage();
      survivor.setHp(damageK);
      return true;
    }
    return true;
  }

  bool isGoal(int x, int y) {
    return maze[x][y] == maze[18][11] && getCurrentCharac() == survivor;
  }

  bool isWin() {
    if (currentCharac.checkWin()) {
      print(
          "\u{2728}Congratulations to the survivors You've reached your goal now.\u{2728}");
      return true;
    }
    return false;
  }

  bool isDaed() {
    if (!currentCharac.checkHP()) {
      var dead;
      if (currentCharac == survivor) {
        dead = "Survivor";
      } else {
        dead = "Killer";
      }
      print(
          "    \u{1F494}\u{1F494}\u{1F494}\u{1F494}\u{1F494}\u{1F494} Game Over \u{1F494}\u{1F494}\u{1F494}\u{1F494}\u{1F494}\u{1F494}\n        ...\u{1F480} $dead is Dead \u{1F480}...");
      return true;
    }
    return false;
  }

  setObjInMap(int x, int y) {
    for (int i = 0; i < objCount; i++) {
      if (object[i].isOnMap(x, y)) {
        maze[object[i].getRow()][object[i].getCol()] = object[i].getSymbol();
      }
    }
  }

  printMap() {
    for (int i = 0; i < 20; i++) {
      for (int j = 0; j < 20; j++) {
        setObjInMap(i, j);
        stdout.write(maze[i][j]);
      }
      print('');
    }
  }

  var bTopUnder = '\u{1F9F1}\u{1F9F1}';
  var bLeftRight = '\u{1F9F1}';
  mazeSetBorder() {
    for (int b = 0; b < 20; b++) {
      if (b == 14) {
        addObj(Wall('\u{1F9F1}', 0, b));
        addObj(Wall('\u{1F9F1}', 19, b));
      } else if (b > 14) {
        addObj(Wall('', 0, b));
        addObj(Wall('', 19, b));
      } else {
        addObj(Wall(bTopUnder, 0, b));
        addObj(Wall(bTopUnder, 19, b));
      }
    }
    for (int b = 1; b < 19; b++) {
      addObj(Wall(bLeftRight, b, 0));
      addObj(Wall(bLeftRight, b, 19));
    }
  }

  var forest = '\u{1F333} ';
  var damageF = Random().nextInt(10) + 2;
  mazeSetForest() {
    addObj(Forest(forest, 4, 15, damageF));
    addObj(Forest(forest, 4, 16, damageF));
    addObj(Forest(forest, 5, 4, damageF));
    addObj(Forest(forest, 5, 5, damageF));
    addObj(Forest(forest, 5, 9, damageF));
    addObj(Forest(forest, 7, 18, damageF));
    addObj(Forest(forest, 11, 6, damageF));
    addObj(Forest(forest, 14, 13, damageF));
    addObj(Forest(forest, 11, 6, damageF));
    addObj(Forest(forest, 18, 1, damageF));
    addObj(Forest(forest, 18, 5, damageF));
    addObj(Forest(forest, 9, 13, damageF));
    addObj(Forest(forest, 9, 17, damageF));
    addObj(Forest(forest, 10, 18, damageF));
    addObj(Forest(forest, 2, 13, damageF));
    addObj(Forest(forest, 3, 13, damageF));
    addObj(Forest(forest, 1, 6, damageF));
    addObj(Forest(forest, 2, 6, damageF));
    addObj(Forest(forest, 1, 7, damageF));
    addObj(Forest(forest, 8, 8, damageF));
    addObj(Forest(forest, 9, 8, damageF));
    addObj(Forest(forest, 9, 7, damageF));
    addObj(Forest(forest, 15, 18, damageF));
    addObj(Forest(forest, 16, 18, damageF));
    addObj(Forest(forest, 16, 11, damageF));
    addObj(Forest(forest, 16, 12, damageF));
    addObj(Forest(forest, 16, 17, damageF));
    addObj(Forest(forest, 9, 3, damageF));
    addObj(Forest(forest, 7, 3, damageF));
    addObj(Forest(forest, 7, 2, damageF));
    addObj(Forest(forest, 16, 5, damageF));
    addObj(Forest(forest, 16, 4, damageF));
    addObj(Forest(forest, 18, 6, damageF));
    addObj(Forest(forest, 17, 9, damageF));
    addObj(Forest(forest, 18, 9, damageF));
    addObj(Forest(forest, 9, 14, damageF));
    addObj(Forest(forest, 9, 15, damageF));
    addObj(Forest(forest, 15, 8, damageF));
    addObj(Forest(forest, 16, 8, damageF));
  }

  var wallH = "\u{1F9F1} ";
  var wallV = "\u{1F9F1} ";
  mazeSetWall() {
    addObj(Wall(wallV, 1, 2));
    addObj(Wall(wallV, 1, 9));
    addObj(Wall(wallV, 1, 14));
    addObj(Wall(wallV, 2, 2));
    addObj(Wall(wallV, 2, 5));
    addObj(Wall(wallV, 2, 7));
    addObj(Wall(wallV, 2, 9));
    addObj(Wall(wallV, 2, 11));
    addObj(Wall(wallV, 2, 14));
    addObj(Wall(wallV, 3, 5));
    addObj(Wall(wallV, 3, 7));
    addObj(Wall(wallV, 3, 9));
    addObj(Wall(wallV, 3, 11));
    addObj(Wall(wallV, 3, 14));
    addObj(Wall(wallV, 3, 16));
    addObj(Wall(wallV, 4, 5));
    addObj(Wall(wallV, 4, 7));
    addObj(Wall(wallV, 4, 9));
    addObj(Wall(wallV, 4, 11));
    addObj(Wall(wallV, 5, 3));
    addObj(Wall(wallV, 6, 3));
    addObj(Wall(wallV, 6, 14));
    addObj(Wall(wallV, 6, 17));
    addObj(Wall(wallV, 7, 5));
    addObj(Wall(wallV, 7, 14));
    addObj(Wall(wallV, 8, 5));
    addObj(Wall(wallV, 8, 12));
    addObj(Wall(wallV, 8, 14));
    addObj(Wall(wallV, 9, 12));
    addObj(Wall(wallV, 9, 16));
    addObj(Wall(wallV, 10, 5));
    addObj(Wall(wallV, 10, 12));
    addObj(Wall(wallV, 11, 2));
    addObj(Wall(wallV, 11, 5));
    addObj(Wall(wallV, 11, 14));
    addObj(Wall(wallV, 11, 16));
    addObj(Wall(wallV, 12, 2));
    addObj(Wall(wallV, 12, 9));
    addObj(Wall(wallV, 12, 12));
    addObj(Wall(wallV, 12, 14));
    addObj(Wall(wallV, 12, 16));
    addObj(Wall(wallV, 13, 2));
    addObj(Wall(wallV, 13, 5));
    addObj(Wall(wallV, 13, 16));
    addObj(Wall(wallV, 14, 12));
    addObj(Wall(wallV, 15, 2));
    addObj(Wall(wallV, 15, 7));
    addObj(Wall(wallV, 15, 10));
    addObj(Wall(wallV, 15, 12));
    addObj(Wall(wallV, 15, 14));
    addObj(Wall(wallV, 16, 2));
    addObj(Wall(wallV, 16, 7));
    addObj(Wall(wallV, 16, 10));
    addObj(Wall(wallV, 16, 14));
    addObj(Wall(wallV, 17, 2));
    addObj(Wall(wallV, 17, 10));
    addObj(Wall(wallV, 17, 12));
    addObj(Wall(wallV, 17, 14));
    addObj(Wall(wallV, 17, 17));
    addObj(Wall(wallV, 18, 10));
    addObj(Wall(wallV, 18, 12));
    addObj(Wall(wallV, 18, 17));
    addObj(Wall(wallH, 2, 3));
    addObj(Wall(wallH, 2, 4));
    addObj(Wall(wallH, 2, 12));
    addObj(Wall(wallH, 2, 16));
    addObj(Wall(wallH, 2, 17));
    addObj(Wall(wallH, 2, 18));
    addObj(Wall(wallH, 4, 1));
    addObj(Wall(wallH, 4, 2));
    addObj(Wall(wallH, 4, 3));
    addObj(Wall(wallH, 4, 13));
    addObj(Wall(wallH, 4, 13));
    addObj(Wall(wallH, 5, 15));
    addObj(Wall(wallH, 5, 16));
    addObj(Wall(wallH, 5, 17));
    addObj(Wall(wallH, 6, 2));
    addObj(Wall(wallH, 6, 5));
    addObj(Wall(wallH, 6, 7));
    addObj(Wall(wallH, 6, 8));
    addObj(Wall(wallH, 6, 9));
    addObj(Wall(wallH, 6, 10));
    addObj(Wall(wallH, 6, 11));
    addObj(Wall(wallH, 6, 12));
    addObj(Wall(wallH, 8, 1));
    addObj(Wall(wallH, 8, 2));
    addObj(Wall(wallH, 8, 3));
    addObj(Wall(wallH, 8, 6));
    addObj(Wall(wallH, 8, 7));
    addObj(Wall(wallH, 8, 9));
    addObj(Wall(wallH, 8, 10));
    addObj(Wall(wallH, 8, 15));
    addObj(Wall(wallH, 8, 16));
    addObj(Wall(wallH, 10, 1));
    addObj(Wall(wallH, 10, 2));
    addObj(Wall(wallH, 10, 4));
    addObj(Wall(wallH, 10, 7));
    addObj(Wall(wallH, 10, 8));
    addObj(Wall(wallH, 10, 9));
    addObj(Wall(wallH, 10, 10));
    addObj(Wall(wallH, 10, 11));
    addObj(Wall(wallH, 11, 17));
    addObj(Wall(wallH, 11, 18));
    addObj(Wall(wallH, 12, 10));
    addObj(Wall(wallH, 12, 11));
    addObj(Wall(wallH, 13, 3));
    addObj(Wall(wallH, 13, 6));
    addObj(Wall(wallH, 13, 7));
    addObj(Wall(wallH, 13, 8));
    addObj(Wall(wallH, 13, 9));
    addObj(Wall(wallH, 14, 14));
    addObj(Wall(wallH, 15, 3));
    addObj(Wall(wallH, 15, 4));
    addObj(Wall(wallH, 15, 5));
    addObj(Wall(wallH, 15, 15));
    addObj(Wall(wallH, 15, 16));
    addObj(Wall(wallH, 15, 17));
    addObj(Wall(wallH, 17, 3));
    addObj(Wall(wallH, 17, 4));
    addObj(Wall(wallH, 17, 6));
    addObj(Wall(wallH, 17, 7));
    addObj(Wall(wallH, 17, 8));
    addObj(Wall(wallH, 17, 16));
    addObj(Wall(wallH, 15, 9));
  }
}
// 0  1  2  3  4  5  6  7  8  9  10 11 12 13 14 15 16 17 18 19
// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX  0
// || K  || -  -  -   -  -  - ||  -  -  -  - ||  -  -  -  S || 1
// || -  || == == ||  - ||  - ||  -  || -  - ||  - == == == || 2
// || -  -  -  -  ||  - ||  - ||  -  || -  - ||  - ||  -  - || 3
// || == == ==  - ||  - ||  - ||  -  || - ==  -  -  -  -  - || 4
// || -  -  ||  -  -  -  -  - FR  -  -  -  -  - == == ==  - || 5
// || - ==  ||  - ==  - == == == == == ==  - ||  -  - ||  - || 6
// || -  -  -   - ||  -  -  -  -  -  -  -  - ||  -  -  - FR || 7
// || == == ==  - || == ==  - == ==  - ||  - || ==  == -  - || 8
// || -  -  -  -  -   -  -  -  -  -  - ||  -  -  -  || -  - || 9
// || == == -  == ||  - == == == == == || == ==  -  -  -  - || 10
// || -  || -  || || FR  -  -  -  -  -  -  - ||  -  ||== == || 11
// || -  || -   -  -  -  -  -  || == == || - ||  -  || -  - || 12
// || -  || == -  || == == ==  == -  -  -  -  -  -  || == - || 13
// || -  -  -  -  -  - -  -  -  - -  - || FR ==  -   - -  - || 14
// || -  || == == == - ||  -  -  ||  - ||  - || == == ==  - || 15
// || -  || -  -  -  - ||  -  -  ||  -  -  - ||  -  -  -  - || 16
// || -  || == == - == == ==  -  ||  - ||  - ||  - == ||  - || 17
// || FR -  -  -  FR -  -  -  -  ||  G ||  -  -  -  - ||  - || 18
// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX 19
