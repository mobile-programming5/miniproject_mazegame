import 'Game.dart';

void main() {
  Game game = Game();
  game.showWelcome();
  game.newMap();
  while (true) {
    game.showMap();
    game.showHp();
    game.showTurn();
    game.inputDirection();
    if (game.isFinish()) {
      break;
    }
  }
}
