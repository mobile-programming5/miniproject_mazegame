import 'dart:io';
import 'dart:math';

import 'Characters.dart';
import 'Maps.dart';

class Game {
  var emoKiller = '\u{1F47F} ';
  var emoSurvivor = '\u{1F625} ';
  late Characters killer;
  late Characters survivor;
  late Maps maps;
  late Characters currentCharac;
  Game() {
    this.maps = new Maps();
    var damage = Random().nextInt(20) + 15;
    var rowK = Random().nextInt(8) + 1; 
    var rowS = Random().nextInt(8) + 1;
    var colK = Random().nextInt(7) + 2; 
    var colS = Random().nextInt(5) + 10;
    this.killer = new Characters(emoKiller, rowK, colK, maps, 100, damage);
    this.survivor = new Characters(emoSurvivor, rowS, colS, maps, 100, 0);
    this.currentCharac = survivor;
  }
  showWelcome() {
    print("\n               ...\u{1FAB4}  WELCOME TO MAZE GAME\u{1FAB4} ...\n");
  }

  newMap() {
    maps.mazeSetBorder();
    maps.mazeSetWall();
    maps.mazeSetForest();
    maps.setCharacter(survivor, killer, currentCharac);
    maps.setGoal();
  }

  showMap() {
    maps.printMap();
  }

  showTurn() {
    Characters character = maps.getCurrentCharac();
    String characterSymbol = character.getSymbol();
    var chars;
    if (maps.isCharacter()) {
      chars = "Killer";
    } else {
      chars = "Survivor";
    }
    print(" > Turn: $chars $characterSymbol ");
  }

  showHp() {
    int hpK = killer.getHp();
    int hpS = survivor.getHp();
    var k = killer.getSymbol();
    var s = survivor.getSymbol();
    print("\n       HP Killer( $k): $hpK || HP Survivor( $s): $hpS");
  }

  bool inputDirection() {
    stdout.write(" > Enter diraction (w,d,a,s): ");
    String inputDirection = stdin.readLineSync()!;
    currentCharac = maps.getCurrentCharac();
    if (currentCharac.walk(inputDirection)) {
      return true;
    }
    return false;
  }

  bool isFinish() {
    if (maps.isWin() || maps.isDaed()) return true;
    return false;
  }
}
