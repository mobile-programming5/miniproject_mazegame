class Obj {
  late int row;
  late int col;
  late String symbol;
  Obj(String symbol, int row, int col) {
    this.symbol = symbol;
    this.row = row;
    this.col = col;
  }
  setRow(int row) {
    this.row = row;
  }

  setCol(int col) {
    this.col = col;
  }

  int getRow() {
    return row;
  }

  int getCol() {
    return col;
  }

  String getSymbol() {
    return symbol;
  }

  bool isOnMap(int row, int col) {
    return this.row == row && this.col == col;
  }
}
