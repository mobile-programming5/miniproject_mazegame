import 'Maps.dart';
import 'Obj.dart';

class Characters extends Obj {
  var emoSurvivor = '\u{1F625} ';
  late Maps maps;
  late int hp;
  late int damage;
  Characters(
      super.symbol, super.row, super.col, Maps maps, int hp, int damage) {
    this.maps = maps;
    this.hp = hp;
    this.damage = damage;
  }
  setHp(int hp) {
    this.hp = hp;
  }

  int getHp() {
    return hp;
  }

  int getDamage() {
    return damage;
  }

  bool canWalk(int x, int y) {
    return !maps.isWall(x, y) &&
        maps.inMap(x, y) &&
        maps.setMaze(row, col) &&
        maps.isForest(x, y) &&
        maps.isSurvivor(x, y) &&
        maps.isKiller(x, y) &&
        maps.getCurrentCharac().getHp() > 0;
  }

  bool checkWin() {
    return maps.isGoal(row, col);
  }

  bool checkHP() {
    return maps.getCurrentCharac().getHp() > 0;
  }

  bool checkDamageKiller(int x, int y) {
    return maps.isSurvivor(x, y);
  }

  bool walk(var direction) {
    switch (direction) {
      case 'W':
      case 'w':
        if (walkN() && !checkWin() && checkHP()) {
          maps.switchCharac();
          return true;
        }
        return false;
      case 'D':
      case 'd':
        if (walkE() && !checkWin() && checkHP()) {
          maps.switchCharac();
          return true;
        }
        return false;
      case 'A':
      case 'a':
        if (walkW() && !checkWin() && checkHP()) {
          maps.switchCharac();
          return true;
        }
        return false;
      case 'S':
      case 's':
        if (walkS() && !checkWin() && checkHP()) {
          maps.switchCharac();
          return true;
        }
        return false;
    }
    return false;
  }

  bool walkN() {
    if (canWalk(row - 1, col)) {
      row -= 1;
      return true;
    }
    printCantWalk();
    return false;
  }

  bool walkE() {
    if (canWalk(row, col + 1)) {
      col += 1;
      return true;
    }
    printCantWalk();
    return false;
  }

  bool walkW() {
    if (canWalk(row, col - 1)) {
      col -= 1;
      return true;
    }
    printCantWalk();
    return false;
  }

  bool walkS() {
    if (canWalk(row + 1, col)) {
      row += 1;
      return true;
    } else {
      printCantWalk();
      return false;
    }
  }

  printCantWalk() {
    var chars;
    if (maps.isCharacter()) {
      chars = "Killer";
    } else {
      chars = "Survivor";
    }
    print("           ...$chars can't walk...\n");
  }
}
